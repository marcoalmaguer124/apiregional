const mongoose = require('mongoose');
const { mongodb } = require('./keys')
mongoose.set('useCreateIndex', true)
//Conectarse a la bd banco regional [esto es solo un test no funcional] ya que solo se cuenta con un login en mongo
mongoose.connect( mongodb.URL, {
    useNewUrlParser: true,
    useUnifiedTopology:true
})
.then(db => console.log('DB conectada'))
.catch(err => console.error('Error en coneccion de DB error:\n'+err))