const express = require("express");
const morgan = require("morgan");
//Inicializar databases banco Regional de un test no funcional en localhost
const app = express();
require('./database')
require('./passport/local-auth')
const passport = require('passport')

//Configuracion
app.set('port', process.env.PORT || 6666);
app.use(express.json());
//Premite la comunicacion por medio de json data
app.use(express.urlencoded({ extended: false })); 

//Middelwares
//Permite ver en consola las peticiones recibidas y status
app.use(morgan('dev'));

//Rutas
app.use('/auth', require('./routes/auth.routes'));
app.use('/',passport.authenticate('jwt',{ session: false }),require('./routes/index.routes'));

//Inicio del servidor BANCO REGIONAL
app.listen(app.get('port'), () => {
    console.log("Server en el puerto ", app.get('port'));
});