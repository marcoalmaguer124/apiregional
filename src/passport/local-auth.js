const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const User = require('../models/user')

passport.use('signup', new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
    //para pasar el resto de parametros en las peticiones
    passReqToCallback: true
}, async(req, email, password, done) => {
    try{
        const newUser = new User({
            email: email,
            password: password,
            username: req.body.username
        });
        await newUser.save();
        done(null, newUser,{message: ["Usuario creado verifica tu correo ..."]})
    }catch(e){
        console.log("Error ")
        if(e.name == "ValidationError"){
            var errors = []
            for (i in e.errors){
                errors.push(e.errors[i].message);
            }
            done(null, false, {message: errors})
        }else{
            done(null, false, {message: ["Error al crear el usuario"]})
        }
    }
}));

passport.use('login', new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: false
},async (email, password, done) => {
    const user = await User.findOne({email: email});
    const genericError = 'Usuario o contraseña no encontrado';
    if(!user){
        done(null, false, {message:genericError});
    }
    if(!user.validatePassword(password)){
        done(null, false, {message:genericError});
    }
    done(null, user)
}));