const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../keys')
const {Strategy, ExtractJwt} = require('passport-jwt');
const passport = require('passport')
const User = require('../models/user')
const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret.secret
};

passport.use('jwt', new Strategy(opts, (payload, done)=> {
    console.log("PAYLOAD:" + payload)
    User.findById(payload.id)
    .then(user => {
        if(user){
          return done(null, {
              id: user.id,
              name: user.username,
              email: user.email,
          });
        }
        return done(null, false);
     }).catch(err =>{ 
        console.error(err);
        return done(null, false);
    });
}));

function createToken(id){
    const token = jwt.sign({id:id}, jwtSecret.secret,{
        expiresIn: 6000,
        algorithm: 'HS256'
    })
    return token;
};

module.exports.createToken = createToken;