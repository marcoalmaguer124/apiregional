module.exports = {
    mongodb: {
        URL: 'mongodb://localhost:27017/RegionalAPITest'
    },
    jwtSecret: {
        secret: 'EstoEsUnTestAPIConNombreDeRegionaldePrueba'
    }
};