const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: function(v) {
                return /^[\w\.%+-]{1,64}@(?:[\w]{1,63}\.){1,125}[\w]{2,63}$/.test(v);
            },
        message: props => 'Es necesario tener un email unico el fomrato correcto'
        }
    },
    password: String,
    username:{
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: function(v) {
                return /^[\w/-/'@/.]{1,30}$/.test(v);
            },
        message: props => 'Es necesario tener un username unico que cumpla con las reglas permitidas de: Menor a 30 caracteres, con letras de la a-z, A-Z, 0-9, _ - . \''
        }
    }
});
//Esto es solo un ejemplo de como cifrar la contraseña con bcrypt de BancoRegional
userSchema.pre('save', async function(next){
    const user = this;
    user.password = await user.encryptPassword(user.password);
    next();
});

userSchema.methods.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
};
userSchema.methods.validatePassword = function (password) {
    return bcrypt.compare(password, this.password);
}

//Schema guardado en collecion llamada user
module.exports = model('User', userSchema) 